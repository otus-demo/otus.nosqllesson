﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Linq;

namespace MongoDB
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // http://media.mongodb.org/zips.json

            var dbClient = new MongoClient("mongodb://localhost:27017");

            IMongoDatabase db = dbClient.GetDatabase("OTUS2");
            var users = db.GetCollection<User>("Users");

            users.InsertOne(new User
            {
                Age = 20,
                User_name = "Alice",
                Phone = "+4991234589",
                _id = Guid.NewGuid().ToString().ToLower(),
                Company = new Company
                {
                    Name = "Otus",
                    Email = "tom@otus.ru",
                    Startwork = DateTime.Now.AddDays(-10)
                }
            }); ;
            
            /*
            var indexKeysDefinition = Builders<User>.IndexKeys.Ascending(x => x.User_name);
            users.Indexes.CreateOne(new CreateIndexModel<User>(indexKeysDefinition));
            */

            var u = users.Find<User>(x => x.Company.Startwork <= DateTime.Now).ToList();

            foreach (var user in u)
            {
                Console.WriteLine(user.User_name + ", " + "age: " + user.Age + " phone: " + user.Phone);
            }

            Console.ReadKey();
        }
    }

    [BsonIgnoreExtraElements]
    public class User
    {
        public string _id { get; set; }
        public string User_name { get; set; }
        public string Phone { get; set; }
        public int Age { get; set; }
        public Company Company { get; set; }
    }

    public class Company
    {
        public string Name { get; set; }
        public string Email { get; set; }
        [BsonDateTimeOptions(Representation = BsonType.String)]
        public DateTime Startwork { get; set; }
    }
}